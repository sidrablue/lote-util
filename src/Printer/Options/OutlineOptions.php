<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Util\Printer\Options;

/**
 * Class OutlineOptions
 * @package SidraBlue\Util\Printer\Options
 */
class OutlineOptions extends Base
{
//Outline Options:
//    --dump-default-toc-xsl          Dump the default TOC xsl style sheet to stdout
//    --dump-outline <file>           Dump the outline to a file
//    --outline                       Put an outline into the pdf (default)
//    --no-outline                    Do not put an outline into the pdf
//    --outline-depth <level>         Set the depth of the outline (default 4)

    /**
     * OutlineOptions default constructor
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * Construct print command for the outline options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        return '';
    }
}