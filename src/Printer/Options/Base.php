<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Util\Printer\Options;

/**
 * Class Base
 * @package SidraBlue\Util\Printer\Options
 */
abstract class Base
{
    /**
     * Construct options command line flags
     *
     * @access protected
     * @return string
     */
    protected abstract function constructCommand();
}