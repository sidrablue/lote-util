<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Util\Printer;

use SidraBlue\Util\Dir;
use SidraBlue\Util\Printer\Options\Base as OptionsBase;
use SidraBlue\Util\Printer\Options\CoverPage;
use SidraBlue\Util\Printer\Options\GlobalOptions;
use SidraBlue\Util\Printer\Options\HeaderFooterOptions;
use SidraBlue\Util\Printer\Options\OutlineOptions;
use SidraBlue\Util\Printer\Options\PageOptions;
use SidraBlue\Util\Printer\Options\TocOptions;

/**
 * Class HtmlToPdf
 * @package Sidrablue\Util\Print
 */
class HtmlToPdf
{
    /**
     * @var string $url - Print URL
     */
    private $url;

    /**
     * @var string $fileName - Print file name
     */
    private $fileName;

    /**
     * @var string $filePath - Print file path
     */
    private $filePath;

    /**
     * @var string $sessionId - the session id
     */
    private $sessionId;

    /**
     * @var GlobalOptions $globalOptions - Global print options
     */
    private $globalOptions = null;

    /**
     * @var OutlineOptions $outlineOptions - Outline print options
     */
    private $outlineOptions = null;

    /**
     * @var PageOptions $pageOptions - Page print options
     */
    private $pageOptions = null;

    /**
     * @var HeaderFooterOptions $headerFooterOptions - Header and footer print options
     */
    private $headerFooterOptions = null;

    /**
     * @var TocOptions $tocOptions - Table of contents print options
     */
    private $tocOptions = null;

    /**
     * @var CoverPage $coverPage - Cover page
     */
    private $coverPage = null;

    public function __construct($url, $fileName, $sessionId = '')
    {
        $this->url = $url;
        $this->fileName = $fileName;

        Dir::make(LOTE_ASSET_PATH . 'temp/');
        $tempName = uniqid('tmp_');
        $pdfFile = LOTE_ASSET_PATH . 'temp/' . $tempName . '.pdf';

        $this->filePath = $pdfFile;
        $this->sessionId = $sessionId;

        $this->setDefaultOptions();
    }

//    /**
//     * Set required print options
//     *
//     * @access public
//     * @param ...OptionsBase $options - Splat of options objects
//     * @return void
//     */
//    public function setOptions(OptionsBase ...$options)
//    {
//        foreach($options as $optionsObject) {
//            if($optionsObject instanceof GlobalOptions) {
//                $this->setGlobalOptions($optionsObject);
//            }
//            if($optionsObject instanceof OutlineOptions) {
//                $this->setGlobalOptions($optionsObject);
//            }
//            if($optionsObject instanceof PageOptions) {
//                $this->setPageOptions($optionsObject);
//            }
//            if($optionsObject instanceof HeaderFooterOptions) {
//                $this->setHeaderFooterOptions($optionsObject);
//            }
//            if($optionsObject instanceof TocOptions) {
//                $this->setTocOptions($optionsObject);
//            }
//        }
//    }

    /**
     * Add global options
     *
     * @access public
     * @param GlobalOptions $options - Global options
     * @return void
     */
    public function setGlobalOptions(GlobalOptions $options)
    {
        $this->globalOptions = $options;
    }

    /**
     * Add outline options
     *
     * @access public
     * @param OutlineOptions $options - Outline options
     * @return void
     */
    public function setOutlineOptions(OutlineOptions $options)
    {
        $this->outlineOptions = $options;
    }

    /**
     * Add page options
     *
     * @access public
     * @param PageOptions $options - Page options
     * @return void
     */
    public function setPageOptions(PageOptions $options)
    {
        $this->pageOptions = $options;
    }

    /**
     * Add header and footer options
     *
     * @access public
     * @param HeaderFooterOptions $options - Header and footer options
     * @return void
     */
    public function setHeaderFooterOptions(HeaderFooterOptions $options)
    {
        $this->headerFooterOptions = $options;
    }

    /**
     * Add table of contents options
     *
     * @access public
     * @param TocOptions $options - Table of contents options
     * @return void
     */
    public function setTocOptions(TocOptions $options)
    {
        $this->tocOptions = $options;
    }

    /**
     * Add cover page
     *
     * @access public
     * @param CoverPage $coverPage - Cover page
     * @return void
     */
    public function setCoverPage(CoverPage $coverPage)
    {
        $this->coverPage = $coverPage;
    }

    /**
     * Print pdf
     *
     * @access public
     * @return void
     * @deprecated Specify either download or inline. printPdf is ambiguous
     * @see HtmlToPdf::downloadPdf()
     * @see HtmlToPdf::displayPdf()
     */
    public function printPdf()
    {
        $command = $this->constructCommand();
        $this->generatePdf($command);
        die;
    }

    /**
     * Print and download pdf
     *
     * @access public
     * @return void
     */
    public function downloadPdf()
    {
        $command = $this->constructCommand();
        $this->generatePdf($command, 'download');
        die;
    }

    /**
     * Print pdf
     *
     * @access public
     * @return void
     */
    public function displayPdf()
    {
        $command = $this->constructCommand();
        $this->generatePdf($command, 'inline');
        die;
    }

    /**
     * Set default options
     *
     * @access protected
     * @return void
     */
    protected function setDefaultOptions()
    {
        $this->setGlobalOptions(new GlobalOptions());
        $this->setOutlineOptions(new OutlineOptions());
        $pageOptions = new PageOptions();
        if($this->sessionId) {
            $pageOptions->addCookie("sessionid", $this->sessionId);
        }
        $this->setPageOptions($pageOptions);
        $this->setHeaderFooterOptions(new HeaderFooterOptions());
        $this->setTocOptions(new TocOptions());
    }

    /**
     * Construct the print command
     *
     * @access protected
     * @return string
     */
    protected function constructCommand()
    {
        $command = "wkhtmltopdf ";

        $command .= $this->globalOptions->constructCommand();
        $command .= $this->outlineOptions->constructCommand();
        $command .= $this->pageOptions->constructCommand();
        $command .= $this->headerFooterOptions->constructCommand();
        $command .= $this->tocOptions->constructCommand();
        if ($this->coverPage) {
            $command .= $this->coverPage->constructCommand();
        }

        $command .= " \"{$this->url}\" {$this->filePath}";
        return $command;
    }

    /**
     * Execute print command and handle output
     *
     * @access protected
     * @param string $command - Print command
     * @param string $disposition - inline|attachment
     * @return void
     */
    protected function generatePdf($command, $disposition = 'attachment')
    {
        shell_exec($command);
        $pdfContent = file_get_contents($this->filePath);

        header('Pragma: public');
        header('Expires: 0');
        header('Content-Type: application/pdf');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Length: ' . strlen($pdfContent));
        header("Content-Disposition: {$disposition};filename=\"{$this->fileName}\"");
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        if ($disposition == 'attachment') {
            header('Content-Type: application/force-download');
            header('Content-Type: application/octet-stream');
            header('Content-Type: application/download');
        }

        ob_clean();
        flush();
        echo $pdfContent;
        unlink($this->filePath);
    }

}