<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Util;

/**
 * Class for XML manipulation utilities.
 */
class Xml
{

    /**
     * Rename a tag in a document
     * @param \DomElement $oldTag
     * @param string $newTagName
     * @access public static
     * @param array $attributesToSet - attributes to add on the renaming
     * @return void
     * */
    public static function renameTag(\DOMElement $oldTag, $newTagName, $attributesToSet = [])
    {
        $newNode = $oldTag->ownerDocument->createElement($newTagName);
        if ($oldTag->attributes->length) {
            foreach ($oldTag->attributes as $attribute) {
                $newNode->setAttribute($attribute->nodeName, $attribute->nodeValue);
            }
            if($attributesToSet) {
                foreach ($attributesToSet as $attName => $attValue) {
                    $newNode->setAttribute($attName, $attValue);
                }
            }
        }
        while ($oldTag->firstChild) {
            $newNode->appendChild($oldTag->firstChild);
        }
        $oldTag->parentNode->replaceChild($newNode, $oldTag);
    }

    /**
     * Parse and XML document to find all tags by a certain name and rename them to a different name
     * @access public static
     * @param string $xmlString - the XML content
     * @param string $tagFrom - the tag from name that we are looking for
     * @param string $tagTo - the name that we will rename the tagFrom to
     * @param $attributeConditions - attributes to meet for this rename to occur
     * @param $attributesToSet - attributes to add
     * @return string
     * */
    public static function renameAllTags($xmlString, $tagFrom, $tagTo, $attributeConditions = [], $attributesToSet = [])
    {
        $xmlString = mb_convert_encoding($xmlString, 'HTML-ENTITIES', 'UTF-8');

        $dom = new \DOMDocument('1.0', 'UTF-8');
        @$dom->loadHTML($xmlString);
        $oldTags = $dom->getElementsByTagName($tagFrom);
        for($i=0; $i<$oldTags->length; $i++) {
            /** @var \DomElement $v */
            $v = $oldTags->item($i);
            if(self::checkAttributeConditions($v,$attributeConditions)) {
                self::renameTag($v, $tagTo, $attributesToSet);
                $xmlString = $v->ownerDocument->saveHTML();
                $i=0;
                $oldTags = $dom->getElementsByTagName($tagFrom);
            }
        }
        return $xmlString;
    }

    /**
     * Check if an element meets a set of attributes
     * @param \DomElement $e
     * @param array $attributes
     * @return bool
     */
    public static function checkAttributeConditions($e, $attributes)
    {
        $result = true;
        if(is_array($attributes) && count($attributes) > 0) {
            foreach($attributes as $k=>$v) {
                if(!$e->hasAttribute($k) || $e->getAttribute($k) != $v) {
                    $result = false;
                    break;
                }
            }
        }
        return $result;
    }

}
