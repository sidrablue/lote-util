<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Util;

use SidraBlue\Lote\Object\Entity\VirtualTypes\Type\StdDateTime;

/**
 * Class for Date manipulation utilities.
 */
Class Date
{

    public static function dateFormat($date, $format)
    {
        $result = '';
        try {
            if (!empty($date)) {
                if ($date instanceof \DateTimeInterface) {
                    $d = $date;
                } else {
                    $d = new \DateTime($date);
                }
                $result = $d->format($format);
            }
        } catch (\Exception $e) {
            if (strlen($date) > 10) {
                $result = self::dateFormat(substr($date, 10), $format);
            }
        }
        return $result;
    }

    public static function validateDate($date)
    {
        /// Assuming date comes in format yyyy-mm-dd
        $result = false;
        if (strlen($date) <= 10) {
            $pieces = explode('-', $date);
            if (count($pieces) == 3) {
                if (is_numeric($pieces[2]) && is_numeric($pieces[1]) && is_numeric($pieces[0])) {
                    $day = $pieces[2];
                    $month = $pieces[1];
                    $year = $pieces[0];
                    $result = checkdate($month, $day, $year); //australian date time format
                }
            }
        }
        return $result;
    }

    public static function validateTime($time)
    {
        /// Assuming time comes in format hh:mm:ss
        $result = false;
        /* time check */
        $pieces = explode(':', $time);
        if (count($pieces) == 3) {
            if (is_numeric($pieces[2]) && is_numeric($pieces[1]) && is_numeric($pieces[0])) {
                $hour = $pieces[0];
                $minute = $pieces[1];
                $second = $pieces[0];
                $result = $hour >= 0 && $hour <= 23 &&
                    $minute >= 0 && $minute <= 59 &&
                    $second >= 0 && $second <= 59;
            }
        }
        return $result;
    }

    /** Requires minimum of custom field branch in framework */
    public static function validateDateTime($dateTime)
    {
        $dateValid = false;
        $timeValid = false;
        /// Assuming datetime coming in is a valid datetime string (yyyy-mm-dd hh:mm:ss)
        if (!empty($dateTime)) {
            $dateTime = trim($dateTime);
            if (strlen($dateTime) <= 19) {
                $pieces = explode(' ', $dateTime);
                $date = $pieces[0];
                $time = $pieces[1];
                $year = strstr($date, '-', true);

                $dateValid = $year >= StdDateTime::MINIMUM_YEAR && self::validateDate($date);
                $timeValid = self::validateTime($time);
            }
        }
        return $dateValid && $timeValid;
    }

    public static function asString($date = 'now', $format = 'c')
    {
        $d = new \DateTime($date);
        return $d->format($format);
    }

    /**
     * Check how many days ago a date occurred
     * @param \DateTime $date
     * @return boolean
     * */
    public static function daysAgo($date)
    {
        $now = Time::getUtcObject('now');
        return $date->diff($now)->format("%a");
    }

    public static function isValidMysql8DateTime(?string $dateTimeString)
    {
        $isValidDateTime = (\DateTime::createFromFormat('Y-m-d H:i:s', $dateTimeString) !== false);
        $isValidDate = (\DateTime::createFromFormat('Y-m-d', $dateTimeString) !== false);

        $isValidTimestamp = (((string)((int)$dateTimeString)) === $dateTimeString) && ($dateTimeString <= PHP_INT_MAX) && ($dateTimeString >= ~PHP_INT_MAX);

        return $dateTimeString && ($isValidDateTime || $isValidDate || $isValidTimestamp);
    }
}
