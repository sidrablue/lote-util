<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Util;

/**
 * Color utilities
 *  */
class Color
{

    /**
     * Get a random hex color
     * @access public
     * @return string
     * */
    public static function getRandomHex()
    {
        return self::getSingleHex(rand(1,1000000));
    }

    /**
     * Get a constant color from a given offset
     * @param int $offset - the color offset
     * @access public
     * @return string
     * */
    public static function getSingleHex($offset)
    {
        return substr(md5($offset), 0, 6);
    }

    /**
     * Get a set of colors
     * @param string $count - the amount of colors to get
     * @param int $brightnessSteps - the brightness offset
     * @access public
     * @return array
     * */
    public static function getSetHex($count, $brightnessSteps = 0)
    {
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $color = self::getSingleHex($i+1);
            if($brightnessSteps) {
                $color = self::adjustBrightness($color, $brightnessSteps);
            }
            $result[] = $color;
        }
        return $result;
    }

    /**
     * Adjust the brightness of a hex color
     * @param string $hex - the color
     * @param int $steps - Steps should be between -255 and 255. Negative = darker, positive = lighter
     * @return string
     * */
    public static function adjustBrightness($hex, $steps)
    {
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex, 0, 1), 2) . str_repeat(substr($hex, 1, 1), 2) . str_repeat(substr($hex, 2, 1), 2);
        }

        // Split into three parts: R, G and B
        $colorParts = str_split($hex, 2);
        $return = '#';

        foreach ($colorParts as $color) {
            $color = hexdec($color); // Convert to decimal
            $color = max(0, min(255, $color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

}
