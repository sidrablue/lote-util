<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Util;

/**
 * Credit Card number utilities.
 *  */
Class CreditCard
{

    /**
     * Check that a provided number is of a valid credit card format
     * @access public
     * @param string $number - the credit card number
     * @return boolean
     * @todo - attribute this correctly
     * */
    public static function validCardNumber($number)
    {
        $number = preg_replace('/\D/', '', $number);

        $number_length = strlen($number);
        $parity = $number_length % 2;

        // Loop through each digit and do the maths
        $total = 0;
        for ($i = 0; $i < $number_length; $i++) {
            $digit = $number[$i];
            // Multiply alternate digits by two
            if ($i % 2 == $parity) {
                $digit *= 2;
                // If the sum is two digits, add them together (in effect)
                if ($digit > 9) {
                    $digit -= 9;
                }
            }
            // Total up the digits
            $total += $digit;
        }
        // If the total mod 10 equals 0, the number is valid
        return ($total % 10 == 0) ? true : false;
    }

}
